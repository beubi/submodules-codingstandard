<?php

namespace Beubi\Sniffs\Commenting;

use PHP_CodeSniffer\Sniffs\Sniff;
use PHP_CodeSniffer\Files\File;

/**
 * Beubi_Sniffs_Commenting_LazyDocBSniff.
 *
 * Warns about Lazy DOzcB comments. This is a modified copy of Generic_Sniffs_Commenting_TodoSniff.
 *
 * @category  PHP
 * @package   PHP_CodeSniffer
 * @author    Hugo Fonseca
 *
 */
class Beubi_Sniffs_Commenting_LazyDocBSniff implements Sniff
{
    /**
     * A list of tokenizers this sniff supports.
     *
     * @var array
     */
    public $supportedTokenizers = ['PHP','JS'];

    /**
     * List of strings that are generated dynamically by docB
     *
     * @var array
     */
    public $toAvoidComments = [
        'Short description for class',
        'Long description for class',
        '@package    package',
        '@subpackage subpackage',
        'Short description - mandatory',
        'Long description - optional',
        '(delete if not)',
        'private/public/protected',
        'returnType returnName Description',
        'Argument description, one @param for each argument if exists'
    ];
    
    /**
     * Returns an array of tokens this test wants to listen for.
     *
     * @return integer[]
     */
    public function register()
    {
        return [T_RETURN];
    }

    /**
     * Processes this sniff, when one of its tokens is encountered.
     *
     * @param File $phpcsFile The file being scanned.
     * @param int  $stackPtr  The position of the current token in the stack passed in $tokens.
     *
     * @return void
     */
    public function process(File $phpcsFile, $stackPtr)
    {
        $tokens = $phpcsFile->getTokens();

        $content = $tokens[$stackPtr]['content'];
        $matches = [];

        foreach ($this->toAvoidComments as $toAvoidComment) {
            $patern = '|[^a-z]+'.$toAvoidComment.'[^a-z]+(.*)|i';
            if (preg_match($patern, $content, $matches) !== 0) {
                // Clear whitespace and some common characters not required at
                // the end of a to-do message to make the warning more informative.
                $type            = 'CommentFound';
                $lazyDocBMessage = trim($matches[0]);
                $lazyDocBMessage = trim($lazyDocBMessage, '[]().');
                $error           = 'LazyDocB foud ~~>';
                $data            = [$lazyDocBMessage];
  
                if ($lazyDocBMessage !== '') {
                    $type   = 'LazyDocB';
                    $error .= ' "%s"';
                }
                $phpcsFile->addWarning($error, $stackPtr, $type, $data);
            }
        }
    }
}
